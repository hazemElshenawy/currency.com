package com.shenawy.spring.application.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "user")
public class User implements Serializable{

	/**
	 * 
	 */

	public User() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private  long id;
	
	@Column(name = "user_name" ,unique=true)
	private String userName;

	@Column(name = "email",unique=true)
	private String email;

	@Column(name = "password")
	private String password;
	
	@Column(name = "first_name")
	private String firstName;

	 
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "active")
	private int active;

	@Column(name = "country")
	private String country;		

 

 
	@Column(name = "timeLogin")
	private Date timeLogin;

	@Column(name = "LASTPASSWORDRESETDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPasswordResetDate;


 
	@Column(name = "IsAdmin")
	private boolean IsAdmin=false;


	@Column(name = "ENABLED")
    private Boolean enabled;

	public long getId() {
		return this.id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getUserName() {
		return this.userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getEmail() {
		return this.email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPassword() {
		return this.password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getFirstName() {
		return this.firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return this.lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public int getActive() {
		return this.active;
	}



	public void setActive(int active) {
		this.active = active;
	}



	public String getCountry() {
		return this.country;
	}



	public void setCountry(String country) {
		this.country = country;
	}






	public Date getLastPasswordResetDate() {
		return this.lastPasswordResetDate;
	}



	public void setLastPasswordResetDate(Date lastPasswordResetDate) {
		this.lastPasswordResetDate = lastPasswordResetDate;
	}



	public boolean isIsAdmin() {
		return this.IsAdmin;
	}



	public void setIsAdmin(boolean isAdmin) {
		this.IsAdmin = isAdmin;
	}



	


	public Boolean getEnabled() {
		return enabled;
	}



	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}



	public Date getTimeLogin() {
		return timeLogin;
	}



	public void setTimeLogin(Date timeLogin) {
		this.timeLogin = timeLogin;
	}

 


	 




}
