/**
 * 
 */
package com.shenawy.spring.application.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author hazem
 *
 */

@Entity
@Table(name = "MC40200")
public class MC40200 implements Serializable{

	 
	@Column(name="CURNCYID",columnDefinition="char(15)")
	private  String CURNCYID;

	@Id	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="CURRNIDX")
	private int CURRNIDX;
	
	
	@Column(name="CRNCYDSC",columnDefinition="char(31)")
	private  String CRNCYDSC;
	
	
	@Column(name="CRNCYDSCA",columnDefinition="char(61)")
	private  String CRNCYDSCA;
	
	
	@Column(name="CRNCYSYM",columnDefinition="char(3)")
	private  String CRNCYSYM;
	
	
	@Column(name="INCLSPAC", columnDefinition="BIT")
	private  boolean INCLSPAC; 
	
	
	
	@Column(name="NEGSYMBL",columnDefinition = "SMALLINT")	
	private short NEGSYMBL;
	
	
	@Column(name="NGSMAMPC",columnDefinition = "SMALLINT")	
	private short NGSMAMPC;
	
	@Column(name="DECSYMBL",columnDefinition = "SMALLINT")	
	private short DECSYMBL;
	
	@Column(name="THOUSSYM",columnDefinition = "SMALLINT")	
	private short THOUSSYM;
	
	
	@Column(name="CURTEXT_1",columnDefinition="char(25)")
	private  String CURTEXT_1;
	
	@Column(name="CURTEXT_2",columnDefinition="char(25)")
	private  String CURTEXT_2;
	
	
	@Column(name="CURTEXT_3",columnDefinition="char(25)")
	private  String CURTEXT_3;
	
	
	@Column(name="CURTEXTA_1",columnDefinition="char(25)")
	private  String CURTEXTA_1;
	
	
	@Column(name="CURTEXTA_2",columnDefinition="char(25)")
	private  String CURTEXTA_2;
	
	
	@Column(name="CURTEXTA_3",columnDefinition="char(25)")
	private  String CURTEXTA_3;
		
	
	@Temporal(value=TemporalType.DATE)
    @Column(name="DEX_ROW_TS")
    private Date DEX_ROW_TS;
	
	
	@Column(name="DEX_ROW_ID")
	private int DEX_ROW_ID;
	
	
	
	public String getCURNCYID() {
		return CURNCYID;
	}

	public void setCURNCYID(String cURNCYID) {
		CURNCYID = cURNCYID;
	}

	public int getCURRNIDX() {
		return CURRNIDX;
	}

	public void setCURRNIDX(int cURRNIDX) {
		CURRNIDX = cURRNIDX;
	}

	public String getCRNCYDSC() {
		return CRNCYDSC;
	}

	public void setCRNCYDSC(String cRNCYDSC) {
		CRNCYDSC = cRNCYDSC;
	}

	public String getCRNCYDSCA() {
		return CRNCYDSCA;
	}

	public void setCRNCYDSCA(String cRNCYDSCA) {
		CRNCYDSCA = cRNCYDSCA;
	}

	public String getCRNCYSYM() {
		return CRNCYSYM;
	}

	public void setCRNCYSYM(String cRNCYSYM) {
		CRNCYSYM = cRNCYSYM;
	}

	public boolean isINCLSPAC() {
		return INCLSPAC;
	}

	public void setINCLSPAC(boolean iNCLSPAC) {
		INCLSPAC = iNCLSPAC;
	}

	public short getNEGSYMBL() {
		return NEGSYMBL;
	}

	public void setNEGSYMBL(short nEGSYMBL) {
		NEGSYMBL = nEGSYMBL;
	}

	public short getNGSMAMPC() {
		return NGSMAMPC;
	}

	public void setNGSMAMPC(short nGSMAMPC) {
		NGSMAMPC = nGSMAMPC;
	}

	public short getDECSYMBL() {
		return DECSYMBL;
	}

	public void setDECSYMBL(short dECSYMBL) {
		DECSYMBL = dECSYMBL;
	}

	public short getTHOUSSYM() {
		return THOUSSYM;
	}

	public void setTHOUSSYM(short tHOUSSYM) {
		THOUSSYM = tHOUSSYM;
	}

	public String getCURTEXT_3() {
		return CURTEXT_3;
	}

	public void setCURTEXT_3(String cURTEXT_3) {
		CURTEXT_3 = cURTEXT_3;
	}

	public String getCURTEXT_2() {
		return CURTEXT_2;
	}

	public void setCURTEXT_2(String cURTEXT_2) {
		CURTEXT_2 = cURTEXT_2;
	}

	public String getCURTEXTA_1() {
		return CURTEXTA_1;
	}

	public void setCURTEXTA_1(String cURTEXTA_1) {
		CURTEXTA_1 = cURTEXTA_1;
	}

	public String getCURTEXTA_2() {
		return CURTEXTA_2;
	}

	public void setCURTEXTA_2(String cURTEXTA_2) {
		CURTEXTA_2 = cURTEXTA_2;
	}

	public String getCURTEXTA_3() {
		return CURTEXTA_3;
	}

	public void setCURTEXTA_3(String cURTEXTA_3) {
		CURTEXTA_3 = cURTEXTA_3;
	}

	public Date getDEX_ROW_TS() {
		return DEX_ROW_TS;
	}

	public void setDEX_ROW_TS(Date dEX_ROW_TS) {
		DEX_ROW_TS = dEX_ROW_TS;
	}

	public int getDEX_ROW_ID() {
		return DEX_ROW_ID;
	}

	public void setDEX_ROW_ID(int dEX_ROW_ID) {
		DEX_ROW_ID = dEX_ROW_ID;
	}

	public String getCURTEXT_1() {
		return CURTEXT_1;
	}

	public void setCURTEXT_1(String cURTEXT_1) {
		CURTEXT_1 = cURTEXT_1;
	}	
	
}
