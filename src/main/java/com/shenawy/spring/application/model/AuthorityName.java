package com.shenawy.spring.application.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}