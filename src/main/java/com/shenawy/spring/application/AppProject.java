/**
 * 
 */
package com.shenawy.spring.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author hazem
 *
 */
@SpringBootApplication
public class AppProject {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(AppProject.class, args);
	}

}
