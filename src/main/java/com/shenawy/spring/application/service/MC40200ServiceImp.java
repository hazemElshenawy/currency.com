/**
 * 
 */
package com.shenawy.spring.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shenawy.spring.application.dao.MC40200Dao;
import com.shenawy.spring.application.model.MC40200;

/**
 * @author hazem
 *
 */
@Service
public class MC40200ServiceImp implements MC40200Service {

	/* (non-Javadoc)
	 * @see com.shenawy.spring.application.service.MC40200Service#getMC40200()
	 */
	@Autowired
	MC40200Dao mC40200Dao;
	
	@Override
	public List<MC40200> getMC40200() {
		// TODO Auto-generated method stub
		return mC40200Dao.getMC40200();
	}

	/* (non-Javadoc)
	 * @see com.shenawy.spring.application.service.MC40200Service#setMC40200(com.shenawy.spring.application.model.MC40200)
	 */
	@Override
	public void setMC40200(MC40200 mC40200) {
		// TODO Auto-generated method stub
		mC40200Dao.setMC40200(mC40200);
	}

}
