/**
 * 
 */
package com.shenawy.spring.application.service;

import java.util.List;

import com.shenawy.spring.application.model.MC40200;

/**
 * @author hazem
 *
 */
public interface MC40200Service {
	public List<MC40200> getMC40200();

	public void setMC40200(MC40200 mC40200);
}
