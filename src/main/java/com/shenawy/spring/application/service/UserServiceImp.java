package com.shenawy.spring.application.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.shenawy.spring.application.dao.UserDao;
import com.shenawy.spring.application.model.User;


@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

 

	@Override
	public void setUser(User user) {
		 
		userDao.setUser(user);
	}

	 

	@Override
	public User getUser(String name, String Pass) {
		// TODO Auto-generated method stub
		return userDao.getUser(name, Pass);
	}

 

	@Override
	public User getUser(String name) {
		// TODO Auto-generated method stub
		return userDao.getUser(name);
	}

	/* (non-Javadoc)
	 * @see com.shenawy.spring.application.service.UserService#getUser()
	 */
	@Override
	public List<User> getUser() {
		// TODO Auto-generated method stub
		return null;
	}

	 

}
