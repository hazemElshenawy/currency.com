package com.shenawy.spring.application.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shenawy.spring.application.jwt.JwtTokenUtil;
import com.shenawy.spring.application.model.MC40200;
import com.shenawy.spring.application.model.User;
import com.shenawy.spring.application.service.MC40200Service;
import com.shenawy.spring.application.service.UserService;

/**
 * Desc-->this controller used to ????
 * 
 * @author matta
 *
 */
@RestController
public class AppController {

	@Autowired
	UserService userService;

	@Autowired
	MC40200Service mC40200Service;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@CrossOrigin
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello() {
		return "Hello World";
	}

	@CrossOrigin
	@RequestMapping(value = "/RegisterUser", method = RequestMethod.POST)
	public boolean RegisterUser(@RequestBody String data) {

		String[] userData = data.split(" ");
		User user = null;

		user = new User();
		String pass = bCryptPasswordEncoder.encode(userData[5]);
		user.setPassword(pass);

		user.setUserName(userData[1]);
		user.setLastName(userData[0]);
		user.setEmail(userData[2]);

		user.setPassword(pass);

		user.setLastPasswordResetDate(new Date());

		boolean flag = true;

		try {
			userService.setUser(user);
		} catch (Exception e) {
			// TODO: handle exception
			flag = false;
		}

		return flag;
	}

	@CrossOrigin
	@RequestMapping(value = "/checkexsitiance", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String checkexsitiance(@RequestBody String usernamePassword) {

		String parts[] = usernamePassword.split(",");

		JSONArray jsonArray = new JSONArray("[" + usernamePassword + "]");

		JSONObject jsonObject = jsonArray.getJSONObject(0);

		User user = userService.getUser(jsonObject.getString("username"), jsonObject.getString("password"));

		Boolean exist = user != null ? true : false;

		String jwt = null;
		if (exist == true) {
			// Reload password post-security so we can generate token
			final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUserName());
			final String token = jwtTokenUtil.generateToken(userDetails);
			jwt = token;
		}

		return jwt;
	}

	@CrossOrigin
	@RequestMapping(value = "/saveCurrency", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean saveCurrency(@RequestBody String CurrencyData) {

		
		
		JSONArray CurrencyDatas = new JSONArray(CurrencyData);
		JSONObject jsonObject = CurrencyDatas.getJSONObject(0);
		
		String username = jwtTokenUtil.getUsernameFromToken(jsonObject.getString("token"));

		User user = userService.getUser(username);
		
		if(user!=null){
			

			System.out.println(jsonObject);

			MC40200 mC40200 = new MC40200();
			mC40200.setCURNCYID(jsonObject.getString("CurrencyID"));
			mC40200.setCRNCYDSC(jsonObject.getString("Description"));
			mC40200.setCRNCYSYM(jsonObject.getString("CurrencySymbol"));
			mC40200.setNEGSYMBL((short) jsonObject.getInt("BeforeAfterAmount"));
			mC40200.setCRNCYDSCA(jsonObject.getString("ArabicDescription"));
			mC40200.setNGSMAMPC((short)Integer.parseInt(jsonObject.getString("sign")));
			mC40200.setINCLSPAC(Boolean.parseBoolean(jsonObject.getString("isChecked")));
			mC40200.setDECSYMBL((short)jsonObject.getInt("decimal"));
			mC40200.setTHOUSSYM((short) jsonObject.getInt("thousands"));
			mC40200.setCURTEXT_1(jsonObject.getString("CurrencyUnit"));
			mC40200.setCURTEXT_2(jsonObject.getString("SubunitConnector"));
			mC40200.setCURTEXT_3(jsonObject.getString("Currencysubunit"));
			mC40200.setCURTEXTA_1(jsonObject.getString("CurrencyUnitArabic"));
			mC40200.setCURTEXTA_2(jsonObject.getString("SubunitConnectorArabic"));
			mC40200.setCURTEXTA_3(jsonObject.getString("CurrencysubunitArabic"));
			mC40200Service.setMC40200(mC40200);
		}

		return true;
	}

	@CrossOrigin
	@RequestMapping(value = "/getCurrency", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public byte[] getCurrency(@RequestBody String token) {

		String username = jwtTokenUtil.getUsernameFromToken(token);

		User user = userService.getUser(username);
		JSONArray CurrencyDatas = new JSONArray();

		if (user != null) {
			List<MC40200> objects = mC40200Service.getMC40200();
			if(objects==null) return null;
			for (Iterator iterator = objects.iterator(); iterator.hasNext();) {
				MC40200 mC40200 = (MC40200) iterator.next();

				JSONObject jsonObject = new JSONObject();
				jsonObject.put("CurrencyID", mC40200.getCURNCYID());
				jsonObject.put("Description", mC40200.getCRNCYDSC());
				jsonObject.put("CurrencySymbol", mC40200.getCRNCYSYM());
				jsonObject.put("BeforeAfterAmount", mC40200.getNEGSYMBL());
				jsonObject.put("ArabicDescription", mC40200.getCRNCYDSCA());
				jsonObject.put("CurrencySymbol", mC40200.getCRNCYSYM());

				CurrencyDatas.put(jsonObject);
			}
		}

		String json = CurrencyDatas.toString(1);

		return json.getBytes();
	};

}
