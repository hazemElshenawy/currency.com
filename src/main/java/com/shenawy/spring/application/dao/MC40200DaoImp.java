/**
 * 
 */
package com.shenawy.spring.application.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.shenawy.spring.application.model.MC40200;
import com.shenawy.spring.application.model.User;

/**
 * @author hazem
 *
 */
@Repository
public class MC40200DaoImp implements MC40200Dao{

	
	@PersistenceContext
	private EntityManager em;
	
	
	
	/* (non-Javadoc)
	 * @see com.shenawy.spring.application.dao.MC40200Dao#getMC40200()
	 */
	@Override
	public List<MC40200> getMC40200() {		
		String hql = "From MC40200 ";
		List<MC40200> mC40200 = null;
		if (!em.createQuery(hql, MC40200.class).getResultList().isEmpty()) {
			mC40200 = em.createQuery(hql, MC40200.class).getResultList();
		}
		
		return mC40200;
	}

	/* (non-Javadoc)
	 * @see com.shenawy.spring.application.dao.MC40200Dao#setMC40200(com.shenawy.spring.application.model.MC40200)
	 */
	@Transactional
	@Override
	public void setMC40200(MC40200 mC40200) {
		// TODO Auto-generated method stub
		em.merge(mC40200);
	}

}
