package com.shenawy.spring.application.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.shenawy.spring.application.model.User;
 
@Repository
public class UserDaoImp implements UserDao {

	@PersistenceContext
	private EntityManager em;
	 

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
 

	 

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUser() {
		String hql = "From User";

		return em.createQuery(hql).getResultList();
	}

	@Transactional
	@Override
	public void setUser(User user) {
		em.merge(user);
	}	

	@Override
	public User getUser(String name, String Pass) {
		String hql = "From User where user_name =? ";
		User user = null;
		String p   = bCryptPasswordEncoder.encode(Pass);
		boolean flag = bCryptPasswordEncoder.matches(Pass, p);
		List<User> users = null;
		
		if(flag==true){
			if (!em.createQuery(hql, User.class).setParameter(1, name).getResultList().isEmpty()) {
				 
				users = em.createQuery(hql, User.class).setParameter(1, name).getResultList();
				for (User user2 : users) {
					 
						user = user2;
					 
				}
			}	
		}
		
		
		
		return user;
	}
 

	@Override
	public User getUser(String name) {
		String hql = "From User where user_name =?";
		User user = null;
		if (!em.createQuery(hql, User.class).setParameter(1, name).getResultList().isEmpty()) {
			user = em.createQuery(hql, User.class).setParameter(1, name).getResultList().get(0);
		}

		return user;
	}
	
  
	 

	 
	 
	
	 


}
