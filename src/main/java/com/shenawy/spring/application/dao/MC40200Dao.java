/**
 * 
 */
package com.shenawy.spring.application.dao;

import java.util.List;

import com.shenawy.spring.application.model.MC40200;

/**
 * @author hazem
 *
 */
public interface MC40200Dao {
	public List<MC40200> getMC40200();

	public void setMC40200(MC40200 mC40200);
}
