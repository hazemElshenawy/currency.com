package com.shenawy.spring.application.dao;

import java.util.List;
import java.util.Map;

import com.shenawy.spring.application.model.User;


public interface UserDao {

	public List<User>getUser();
	
	public void setUser(User user);
	
	public User getUser(String name,String pass);
	public User getUser(String name );
 
	 
	
}
